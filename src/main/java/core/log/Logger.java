package core.log;

import core.SQLBuilder;

import java.time.LocalTime;

public class Logger {
    private final LocalTime time;
    private SQLBuilder builder;

    public Logger(SQLBuilder builder, boolean state) {
        this.builder = builder;
        this.time = LocalTime.now();
    }

    private String time() {
        return String.format("%s:%s:%s", time.getHour(), time.getMinute(), time.getSecond());
    }
}
