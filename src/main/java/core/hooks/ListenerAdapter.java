package core.hooks;

import core.events.TransactionEvent;
import org.jetbrains.annotations.NotNull;

public class ListenerAdapter implements EventListener {

    public void onDataTransEvent(@NotNull TransactionEvent event) {}

}
