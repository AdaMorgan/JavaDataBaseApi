package core.connect;

import org.slf4j.helpers.CheckReturnValue;
import reactor.util.annotation.NonNull;

public interface Query {

    @NonNull
    @CheckReturnValue
    default Query sendQuery(@NonNull CharSequence value) {
        return this;
    }

    @NonNull
    @CheckReturnValue
    default Query sendQuery(@NonNull String value) {
        return this;
    }
}
