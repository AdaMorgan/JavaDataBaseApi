package core.exceptions;

import core.SQLBuilder;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.Iterator;

public class InvalidAuthException extends Exception implements Iterable<Throwable> {
    public InvalidAuthException(SQLBuilder builder) {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            if (throwable instanceof SQLException) System.out.println(throwable.getMessage());
        });
    }

    @NotNull
    @Override
    public Iterator<Throwable> iterator() {
        return null;
    }
}
